import React, { useState } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import { Map, Modal, Panel, Input, List } from "./components/Index"

export default function App() {
  const [puntos, setPuntos] = useState([])
  const [nombre, setNombre] = useState("")
  const [puntoTemp, setPuntoTemp] = useState({})
  const [visibilityFilter, setVisisbilityFilter] = useState('new_punto')
  const [visibility, setVisisbility] = useState(false)
  const [pointsFilter, setPointsFilters] = useState(true)

  const togglePointsFilter = () => setPointsFilters(!pointsFilter)

  const handleLongPress = ({ nativeEvent }) => {
    setVisisbilityFilter('new_punto')
    setPuntoTemp(nativeEvent.coordinate)
    setVisisbility(true)
  }

  const handleChangeText = text => {
    setNombre(text)
  }

  const handleSubmit = () => {
    const newPunto = { coordinate: puntoTemp, name: nombre }
    setPuntos(puntos.concat(newPunto))
    setVisisbility(false)
    setNombre('')
  }

  const handleCancelar = () => {
    setPuntoTemp({})
    setNombre('')
    setVisisbility(false)
  }

  const handleLista = () => {
    setVisisbilityFilter('all_puntos')
    setVisisbility(true)
  }

  return (
    <View style={styles.container}>
      <Map
        onLongPress={handleLongPress}
        puntos={puntos}
        pointsFilter={pointsFilter}
      />
      <Modal visibility={visibility}>
        {visibilityFilter === 'new_punto' 
          ? 
          <View style={styles.form}>
            <Input
              title="Nombre"
              placeholder="Nombre del punto"
              onChangeText={handleChangeText}
            />
            <View style={styles.buttons}>
              <Button title="Cancelar" onPress={handleCancelar} />
              <Button title="Aceptar" onPress={handleSubmit} />
            </View>
          </View>
          : <List puntos={puntos} closeModal={() => setVisisbility(false)} />
        }
      </Modal>
      <Panel
        onPressLeft={handleLista}
        textLeft={'Lista'}
        togglePointsFilter={togglePointsFilter}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: "center",
    backgroundColor: '#ecf0f1',
  },
  form: {
    padding: 20,
  },
  buttons: {
    marginTop: 50,
    flexDirection: "row",
    justifyContent: "space-between"
  }
});
