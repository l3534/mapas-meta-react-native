import React, { useState } from "react";
import { Text, View, TextInput, StyleSheet } from "react-native";

export default ({ title, ...rest }) => {
  return (
    <View style={styles.wrapper}>
      <Text>{title}</Text>
      <TextInput style={styles.input} {...rest} />
    </View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    height: 80,
  },
  input: {
    marginTop: 30,
  }
})
